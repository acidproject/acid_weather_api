import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import indexRouter from './routes/index'
import weatherRouter from './routes/weather'

let app = express();

// settings
app.set('port', process.env.port || 3005)
app.set('json spaces', 2);

// Routes
app.use(indexRouter);
app.use('/api/movies', weatherRouter);

//middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

app.listen(app.get('port'), () => {
    console.log("server running")
})