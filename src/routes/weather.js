import {Router} from 'express';
import {getWeatherByCountryName} from '../controllers/weathersController'
let router = Router();

router.get('/', getWeatherByCountryName);

export default router;